<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    echo "<h3>API development maleskuliah.com</h3>";
});

//email
$router->group(['prefix' => 'email', 'namespace' => 'Email'], function () use ($router) {
	$router->get('/', ['uses' => 'EmailController@index']);
});

$router->group(['prefix' => 'v1'], function () use ($router) {
	//seminar
	$router->group(['prefix' => 'seminar', 'namespace' => 'Seminar'], function () use ($router) {
		$router->get('/', ['uses' => 'SeminarController@index']);
		$router->get('/detail/{id}', ['uses' => 'SeminarController@show']);
		$router->get('/web/detail/{id}', ['uses' => 'SeminarController@show_web']);
		$router->get('categories', ['uses' => 'CategoryController@index']);
	});

	//news
	$router->group(['prefix' => 'news', 'namespace' => 'News'], function () use ($router) {
		$router->get('/', ['uses' => 'NewsController@index']);
		$router->get('/detail/{id}', ['uses' => 'NewsController@show']);
		$router->get('/web/detail/{id}', ['uses' => 'NewsController@show_web']);
		$router->get('categories', ['uses' => 'CategoryController@index']);
	});

	//users
	$router->group(['prefix' => 'user', 'namespace' => 'Users'], function () use ($router) {
        $router->post('/', ['uses' => 'UserController@store']);
	});

    //forgot password
    $router->group(['prefix' => 'password', 'namespace' => 'Users'], function () use ($router) {
        $router->post('/forgot', ['uses' => 'UserController@forgotPassword']);
        $router->post('/new', ['uses' => 'UserController@newPassword']);
    });

	//faqs
	$router->group(['prefix' => 'faqs', 'namespace' => 'Faq'], function () use ($router) {
		$router->get('/', ['uses' => 'FaqController@index']);
		$router->get('/detail/{id}', ['uses' => 'FaqController@show']);
	});

	//tos
	$router->group(['prefix' => 'tos', 'namespace' => 'Tos'], function () use ($router) {
		$router->get('/', ['uses' => 'TosController@index']);
		$router->get('/mobile', ['uses' => 'TosController@mobile']);
		$router->get('/detail/{id}', ['uses' => 'TosController@show']);
	});

	//cms
	$router->group(['prefix' => 'cms', 'namespace' => 'Cms'], function () use ($router) {
		$router->get('/', ['uses' => 'CmsController@index']);
		$router->get('/mobile', ['uses' => 'CmsController@mobile']);
	});

	//instantion
	$router->group(['prefix' => 'instantion', 'namespace' => 'Master'], function () use ($router) {
		$router->get('/', ['uses' => 'InstantionController@index']);
	});

	//payment
	$router->group(['prefix' => 'payment', 'namespace' => 'Master'], function () use ($router) {
		$router->get('/', ['uses' => 'PaymentController@index']);
	});

	//team
	$router->group(['prefix' => 'team', 'namespace' => 'Cms'], function () use ($router) {
		$router->get('/', ['uses' => 'TeamController@index']);
	});

	//career
	$router->group(['prefix' => 'career', 'namespace' => 'Cms'], function () use ($router) {
		$router->get('/', ['uses' => 'CareerController@index']);
		$router->post('/apply/{id}', ['uses' => 'CareerController@apply']);
		$router->get('/detail/{id}', ['uses' => 'CareerController@show']);
	});

	//province
	$router->group(['prefix' => 'provinces', 'namespace' => 'Master'], function () use ($router) {
		$router->get('/', ['uses' => 'ProvinceController@index']);
	});

	//city
	$router->group(['prefix' => 'cities', 'namespace' => 'Master'], function () use ($router) {
		$router->get('/', ['uses' => 'CityController@index']);
	});

	//customer
	$router->group(['prefix' => 'customer', 'namespace' => 'Transaction'], function () use ($router) {
		$router->get('/', ['uses' => 'TransactionController@customer']);
	});

	//Voucher
	$router->group(['prefix' => 'voucher', 'namespace' => 'Transaction'], function () use ($router) {
		$router->get('/', ['uses' => 'TransactionController@voucher']);
		$router->post('/reedem', ['uses' => 'TransactionController@reedem']);
	});

	//homepage
	$router->group(['prefix' => 'home', 'namespace' => 'Homepage'], function () use ($router) {
		$router->get('/mobile', ['uses' => 'HomepageController@mobile']);
		$router->get('/web', ['uses' => 'HomepageController@web']);
	});

	//transaction
	$router->group(['prefix' => 'transaction', 'namespace' => 'Transaction'], function () use ($router) {
		$router->get('/', ['uses' => 'TransactionController@index']);
		$router->get('/detail/{id}', ['uses' => 'TransactionController@detail']);
		$router->post('/checkout', ['uses' => 'TransactionController@checkout']);
		$router->post('/confirmation/{id}', ['uses' => 'TransactionController@confirmation']);
	});

	//login sosmed
	$router->group(['prefix' => 'login', 'namespace' => 'Users'], function () use ($router) {
		$router->post('/sosmed', ['uses' => 'UserController@loginSosmed']);
	});

	//with auth
	$router->group(['middleware' => 'auth'], function() use ($router) {
		//profile
		$router->group(['prefix' => 'profile', 'namespace' => 'Users'], function () use ($router) {
			$router->get('/', ['uses' => 'UserController@profile']);
			$router->post('/update', ['uses' => 'UserController@update']);
		});

		//token fcm
		$router->group(['prefix' => 'fcm', 'namespace' => 'Users'], function () use ($router) {
			$router->post('/', ['uses' => 'UserController@updateFcm']);
		});

		//password
		$router->group(['prefix' => 'password', 'namespace' => 'Users'], function () use ($router) {
			$router->post('/change', ['uses' => 'UserController@password']);
		});

		//comment
		$router->group(['prefix' => 'news', 'namespace' => 'News'], function () use ($router) {
			$router->post('/comment/{id}', ['uses' => 'NewsController@comment']);
		});

		//notification
		$router->group(['prefix' => 'notification', 'namespace' => 'Notification'], function () use ($router) {
			$router->get('/', ['uses' => 'NotificationController@index']);
			$router->get('/detail/{id}', ['uses' => 'NotificationController@show']);
		});

		//transaction
		$router->group(['prefix' => 'seminar', 'namespace' => 'Transaction'], function () use ($router) {
			$router->get('/scan', ['uses' => 'TransactionController@scan']);
			$router->get('/participant/{id}', ['uses' => 'TransactionController@participant']);
			$router->post('/absent/{id}', ['uses' => 'TransactionController@absent']);
		});
	});
});


$router->group(['prefix' => 'dev'], function () use ($router) {
	//seminar
	$router->group(['prefix' => 'seminar', 'namespace' => 'Seminar'], function () use ($router) {
		$router->get('/', ['uses' => 'SeminarController@index']);
		$router->get('/detail/{id}', ['uses' => 'SeminarController@show']);
		$router->get('/web/detail/{id}', ['uses' => 'SeminarController@show_web']);
		$router->get('categories', ['uses' => 'CategoryController@index']);
	});

	//news
	$router->group(['prefix' => 'news', 'namespace' => 'News'], function () use ($router) {
		$router->get('/', ['uses' => 'NewsController@index']);
		$router->get('/detail/{id}', ['uses' => 'NewsController@show']);
		$router->get('/web/detail/{id}', ['uses' => 'NewsController@show_web']);
		$router->get('categories', ['uses' => 'CategoryController@index']);
	});

	//users
	$router->group(['prefix' => 'user', 'namespace' => 'Users'], function () use ($router) {
        $router->post('/', ['uses' => 'UserController@store']);
	});

    //forgot password
    $router->group(['prefix' => 'password', 'namespace' => 'Users'], function () use ($router) {
        $router->post('/forgot', ['uses' => 'UserController@forgotPassword']);
        $router->post('/new', ['uses' => 'UserController@newPassword']);
    });

	//faqs
	$router->group(['prefix' => 'faqs', 'namespace' => 'Faq'], function () use ($router) {
		$router->get('/', ['uses' => 'FaqController@index']);
		$router->get('/detail/{id}', ['uses' => 'FaqController@show']);
	});

	//tos
	$router->group(['prefix' => 'tos', 'namespace' => 'Tos'], function () use ($router) {
		$router->get('/', ['uses' => 'TosController@index']);
		$router->get('/mobile', ['uses' => 'TosController@mobile']);
		$router->get('/detail/{id}', ['uses' => 'TosController@show']);
	});

	//cms
	$router->group(['prefix' => 'cms', 'namespace' => 'Cms'], function () use ($router) {
		$router->get('/', ['uses' => 'CmsController@index']);
		$router->get('/mobile', ['uses' => 'CmsController@mobile']);
	});

	//instantion
	$router->group(['prefix' => 'instantion', 'namespace' => 'Master'], function () use ($router) {
		$router->get('/', ['uses' => 'InstantionController@index']);
	});

	//payment
	$router->group(['prefix' => 'payment', 'namespace' => 'Master'], function () use ($router) {
		$router->get('/', ['uses' => 'PaymentController@index']);
	});

	//team
	$router->group(['prefix' => 'team', 'namespace' => 'Cms'], function () use ($router) {
		$router->get('/', ['uses' => 'TeamController@index']);
	});

	//career
	$router->group(['prefix' => 'career', 'namespace' => 'Cms'], function () use ($router) {
		$router->get('/', ['uses' => 'CareerController@index']);
		$router->post('/apply/{id}', ['uses' => 'CareerController@apply']);
		$router->get('/detail/{id}', ['uses' => 'CareerController@show']);
	});

	//province
	$router->group(['prefix' => 'provinces', 'namespace' => 'Master'], function () use ($router) {
		$router->get('/', ['uses' => 'ProvinceController@index']);
	});

	//city
	$router->group(['prefix' => 'cities', 'namespace' => 'Master'], function () use ($router) {
		$router->get('/', ['uses' => 'CityController@index']);
	});

	//customer
	$router->group(['prefix' => 'customer', 'namespace' => 'Transaction'], function () use ($router) {
		$router->get('/', ['uses' => 'TransactionController@customer']);
	});

	//Voucher
	$router->group(['prefix' => 'voucher', 'namespace' => 'Transaction'], function () use ($router) {
		$router->get('/', ['uses' => 'TransactionController@voucher']);
		$router->post('/reedem', ['uses' => 'TransactionController@reedem']);
	});

	//homepage
	$router->group(['prefix' => 'home', 'namespace' => 'Homepage'], function () use ($router) {
		$router->get('/mobile', ['uses' => 'HomepageController@mobile']);
		$router->get('/web', ['uses' => 'HomepageController@web']);
	});

	//transaction
	$router->group(['prefix' => 'transaction', 'namespace' => 'Transaction'], function () use ($router) {
		$router->get('/', ['uses' => 'TransactionController@index']);
		$router->get('/detail/{id}', ['uses' => 'TransactionController@detail']);
		$router->post('/checkout', ['uses' => 'TransactionController@checkout']);
		$router->post('/confirmation/{id}', ['uses' => 'TransactionController@confirmation']);
	});

	//login sosmed
	$router->group(['prefix' => 'login', 'namespace' => 'Users'], function () use ($router) {
		$router->post('/sosmed', ['uses' => 'UserController@loginSosmed']);
	});

	//with auth
	$router->group(['middleware' => 'auth'], function() use ($router) {
		//profile
		$router->group(['prefix' => 'profile', 'namespace' => 'Users'], function () use ($router) {
			$router->get('/', ['uses' => 'UserController@profile']);
			$router->post('/update', ['uses' => 'UserController@update']);
		});

		//token fcm
		$router->group(['prefix' => 'fcm', 'namespace' => 'Users'], function () use ($router) {
			$router->post('/', ['uses' => 'UserController@updateFcm']);
		});

		//password
		$router->group(['prefix' => 'password', 'namespace' => 'Users'], function () use ($router) {
			$router->post('/change', ['uses' => 'UserController@password']);
		});

		//comment
		$router->group(['prefix' => 'news', 'namespace' => 'News'], function () use ($router) {
			$router->post('/comment/{id}', ['uses' => 'NewsController@comment']);
		});

		//notification
		$router->group(['prefix' => 'notification', 'namespace' => 'Notification'], function () use ($router) {
			$router->get('/', ['uses' => 'NotificationController@index']);
			$router->get('/detail/{id}', ['uses' => 'NotificationController@show']);
		});

		//transaction
		$router->group(['prefix' => 'seminar', 'namespace' => 'Transaction'], function () use ($router) {
			$router->get('/scan', ['uses' => 'TransactionController@scan']);
			$router->get('/participant/{id}', ['uses' => 'TransactionController@participant']);
			$router->post('/absent/{id}', ['uses' => 'TransactionController@absent']);
		});
	});
});
