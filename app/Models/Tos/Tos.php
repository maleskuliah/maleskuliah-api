<?php

namespace App\Models\Tos;

use Illuminate\Database\Eloquent\Model;

class Tos extends Model 
{
	protected $table = "mls_tos";
}