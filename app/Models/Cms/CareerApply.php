<?php

namespace App\Models\Cms;

use Illuminate\Database\Eloquent\Model;

class CareerApply extends Model 
{
	protected $table = "mls_apply_jobs";
}