<?php

namespace App\Models\Cms;

use Illuminate\Database\Eloquent\Model;

class CareerCategory extends Model 
{
	protected $table = "mls_career_categories";
}