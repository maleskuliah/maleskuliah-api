<?php

namespace App\Models\Cms;

use Illuminate\Database\Eloquent\Model;

class Team extends Model 
{
	protected $table = "mls_teams";
}