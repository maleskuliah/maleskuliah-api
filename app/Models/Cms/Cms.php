<?php

namespace App\Models\Cms;

use Illuminate\Database\Eloquent\Model;

class Cms extends Model 
{
	protected $table = "mls_cms";
}