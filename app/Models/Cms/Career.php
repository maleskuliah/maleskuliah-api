<?php

namespace App\Models\Cms;

use Illuminate\Database\Eloquent\Model;

class Career extends Model 
{
	protected $table = "mls_careers";
}