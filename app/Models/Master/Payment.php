<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model 
{
	protected $table = "mls_payments";
}