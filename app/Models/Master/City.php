<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class City extends Model 
{
	protected $table = "mls_cities";

	public function province()
    {
        return $this->belongsTo('App\Models\Master\Province','province_id','id');
    }
}