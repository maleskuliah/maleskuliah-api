<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Province extends Model 
{
	protected $table = "mls_provinces";
}