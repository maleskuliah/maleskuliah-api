<?php

namespace App\Models\Faq;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model 
{
	protected $table = "mls_faqs";
}