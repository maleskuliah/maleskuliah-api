<?php

namespace App\Models\News;

use Illuminate\Database\Eloquent\Model;

class Category extends Model 
{
	protected $table = "mls_news_categories";
}