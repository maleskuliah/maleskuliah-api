<?php

namespace App\Models\News;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model 
{
	protected $table = "mls_comments";

	public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
}