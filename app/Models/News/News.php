<?php

namespace App\Models\News;

use Illuminate\Database\Eloquent\Model;

class News extends Model 
{
	protected $table = "mls_news";

	public function category()
    {
        return $this->belongsTo('App\Models\News\Category','category_id','id');
    }

    public function writer()
    {
        return $this->belongsTo('App\User','writer_id','id');
    }
}