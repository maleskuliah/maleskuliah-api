<?php

namespace App\Models\Seminar;

use Illuminate\Database\Eloquent\Model;

class Price extends Model 
{
	protected $table = "mls_seminar_prices";

	public function seminar()
    {
        return $this->belongsTo('App\Models\Seminar','seminar_id','id');
    }
}