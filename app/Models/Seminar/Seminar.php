<?php

namespace App\Models\Seminar;

use Illuminate\Database\Eloquent\Model;

class Seminar extends Model 
{
	protected $table = "mls_seminars";

	public function category()
    {
        return $this->belongsTo('App\Models\Seminar\Category','category_id','id');
    }

    public function organizer()
    {
        return $this->belongsTo('App\User','organizer_id','id');
    }
}