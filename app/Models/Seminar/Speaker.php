<?php

namespace App\Models\Seminar;

use Illuminate\Database\Eloquent\Model;

class Speaker extends Model 
{
	protected $table = "mls_speakers";
}