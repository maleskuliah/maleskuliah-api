<?php

namespace App\Models\Seminar;

use Illuminate\Database\Eloquent\Model;

class Category extends Model 
{
	protected $table = "mls_seminar_categories";
}