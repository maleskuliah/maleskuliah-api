<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model 
{
	protected $table = "mls_transactions";

	public function seminar()
    {
        return $this->belongsTo('App\Models\Seminar\Seminar','seminar_id','id');
    }

    public function buyer()
    {
        return $this->belongsTo('App\User','buyer_id','id');
    }

    public function payment()
    {
        return $this->belongsTo('App\Models\Master\Payment','payment_id','id');
    }
}