<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model 
{
	protected $table = "mls_transaction_details";
}