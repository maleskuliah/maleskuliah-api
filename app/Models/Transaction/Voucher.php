<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model 
{
	protected $table = "mls_vouchers";
}