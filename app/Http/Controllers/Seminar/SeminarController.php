<?php

namespace App\Http\Controllers\Seminar;

use Illuminate\Http\Request;
use App\Models\Seminar\Seminar;
use App\Models\Seminar\Price;
use App\Models\Seminar\Speaker;
use App\Models\Master\City;
use Laravel\Lumen\Routing\Controller as BaseController;

class SeminarController extends BaseController
{

    public function __construct()
    {
        $this->cdn  = config('app.cdn');
        $this->user = \Auth::user();
        $this->frontEndUrl = "https://maleskuliah.com/";
    }
    
    public function index(Request $request)
    {
        $data       = array();

        $query     = Seminar::orderBy('created_at', 'DESC');

        //if use search on homepage with category
        if($request->has('category_id') && $request->category_id != NULL)
            $query->where('category_id', $request->category_id);

        //if use search on homepage with instantion
        if($request->has('instantion_id') && $request->instantion_id != NULL)
            $query->where('instantion_id', $request->instantion_id);

        //if use search on homepage with city
        if($request->has('city_id') && $request->city_id != NULL)
            $query->where('city_id', $request->city_id);

        //if use search on homepage with title
        if($request->has('title') && $request->title != NULL)
            $query->where('title', 'like', '%'. $request->title .'%');

        //if use search on homepage with tags
        if($request->has('tag') && $request->tag != NULL)
            $query->where('tags', 'like', '%'. $request->tag .'%');
        
        //if use search on homepage with city
        if($request->has('city') && $request->city != NULL) {
            $city_id = City::where('name','like','%'. $request->city .'%')->pluck('id');
            
            if(sizeof($city_id) > 0) {
                $query->whereIn('city_id', $city_id);
            }
        }

        if($this->user)
            $query->where('instantion_id', $this->user->instantion_id);

        $seminar    = $query->get();

        if(sizeof($seminar) > 0) {
            foreach ($seminar as $key => $value) {

                //get price
                $price          = null;
                $seminarPrice   = Price::where('seminar_id', $value->id)->get();

                if($seminarPrice) {
                    foreach ($seminarPrice as $index => $item) {
                        $price[$index]['id']    = $item->id;
                        $price[$index]['type']  = $item->type;
                        $price[$index]['price'] = $item->price;
                        $price[$index]['label'] = $item->description;
                    }
                }

                $data[$key]['id']           = $value->id;
                $data[$key]['organizer']    = isset($value->organizer) ? $value->organizer->fullname : "";
                $data[$key]['category']     = isset($value->category) ? $value->category->category_name : "";
                $data[$key]['title']        = $value->title;
                $data[$key]['description']  = $value->description;
                $data[$key]['price']        = $price;
                $data[$key]['address']      = $value->address;
                $data[$key]['latitude']     = $value->latitude;
                $data[$key]['longitude']    = $value->longitude;
                $data[$key]['date']         = $value->date;
                $data[$key]['start_time']   = $value->start_time;
                $data[$key]['end_time']     = $value->end_time;
                $data[$key]['tags']         = $value->tags;
                $data[$key]['image']        = $value->image == null ? "" : $this->cdn."/seminars/".$value->image;
                $data[$key]['created_at']   = date_format(date_create($value->created_at), 'Y-m-d  H:i:s');
                $data[$key]['updated_at']   = date_format(date_create($value->updated_at), 'Y-m-d  H:i:s');

                $speaker = Speaker::where('seminar_id', $value->id)->get();

                foreach ($speaker as $key2 => $value2) {
                    $data[$key]['speakers'][$key2]['id']            = $value2->id;
                    $data[$key]['speakers'][$key2]['name']          = $value2->name;
                    $data[$key]['speakers'][$key2]['title']         = $value2->title;
                    $data[$key]['speakers'][$key2]['material']      = $value2->seminar_material;
                    $data[$key]['speakers'][$key2]['created_at']    = date_format(date_create($value2->created_at), 'Y-m-d  H:i:s');
                    $data[$key]['speakers'][$key2]['updated_at']    = date_format(date_create($value2->updated_at), 'Y-m-d  H:i:s');
                }
            }
        }

        $message = sizeof($data) == 0 ? "Data seminar tidak tersedia." : "Berhasil mengambil data seminar";

        if(!$seminar) {
            $message = "Gagal mengambil data seminar";
        }
        
        return response()->json([ 'message' => $message, 'data' => $data ]);
    }

    public function show($id)
    {
        $data    = null;
        $seminar = Seminar::find($id);

        if($seminar) {

            //get price
            $price          = null;
            $seminarPrice   = Price::where('seminar_id', $id)->get();

            if($seminarPrice) {
                foreach ($seminarPrice as $index => $item) {
                    $price[$index]['id']    = $item->id;
                    $price[$index]['type']  = $item->type;
                    $price[$index]['price'] = $item->price;
                    $price[$index]['label'] = $item->description;
                }
            }

            $data['id']                 = $seminar->id;
            $data['organizer']          = isset($seminar->organizer) ? $seminar->organizer->fullname : "";
            $data['category']           = isset($seminar->category) ? $seminar->category->category_name : "";
            $data['title']              = $seminar->title;
            $data['description']        = $seminar->description;
            $data['price']              = $price;
            $data['address']            = $seminar->address;
            $data['latitude']           = $seminar->latitude;
            $data['longitude']          = $seminar->longitude;
            $data['date']               = $seminar->date;
            $data['start_time']         = $seminar->start_time;
            $data['end_time']           = $seminar->end_time;
            $data['tags']               = $seminar->tags;
            $data['image']              = $seminar->image == null ? "" : $this->cdn."/seminars/".$seminar->image;

            $title                      = strtolower(str_replace(" ", "-", $seminar->title));

            $data['link']               = $this->frontEndUrl."seminar/".$title;
            $data['register_end_date']  = date_format(date_create($seminar->deadline), 'Y-m-d');
            $data['register_end_time']  = date_format(date_create($seminar->deadline), 'H:i:s');
            $data['created_at']         = date_format(date_create($seminar->created_at), 'Y-m-d  H:i:s');
            $data['updated_at']         = date_format(date_create($seminar->updated_at), 'Y-m-d  H:i:s');

            $speaker = Speaker::where('seminar_id', $seminar->id)->get();

            foreach ($speaker as $key2 => $value2) {
                $data['speakers'][$key2]['id']            = $value2->id;
                $data['speakers'][$key2]['name']          = $value2->name;
                $data['speakers'][$key2]['title']         = $value2->title;
                $data['speakers'][$key2]['material']      = $value2->seminar_material;
                $data['speakers'][$key2]['created_at']    = date_format(date_create($value2->created_at), 'Y-m-d  H:i:s');
                $data['speakers'][$key2]['updated_at']    = date_format(date_create($value2->updated_at), 'Y-m-d  H:i:s');
            }
        }

        $message = "Berhasil mengambil data detail seminar";

        if(!$seminar) {
            $message = !isset($seminar) ? "Data seminar tidak tersedia." : "Gagal mengambil data detail seminar";
        }

        return response()->json([ 'message' => $message, 'data' => $data ]);
    }

    public function show_web($id)
    {
        $data    = null;

        $title   = str_replace("-", " ", ucwords($id));
        $seminar = Seminar::where('title', 'like', '%'.$title.'%')->first();

        if($seminar) {

            //get price
            $price          = null;
            $seminarPrice   = Price::where('seminar_id', $seminar->id)->get();

            if($seminarPrice) {
                foreach ($seminarPrice as $index => $item) {
                    $price[$index]['id']    = $item->id;
                    $price[$index]['type']  = $item->type;
                    $price[$index]['price'] = $item->price;
                    $price[$index]['label'] = $item->description;
                }
            }
            
            $data['id']           = $seminar->id;
            $data['organizer']    = isset($seminar->organizer) ? $seminar->organizer->fullname : "";
            $data['category']     = isset($seminar->category) ? $seminar->category->category_name : "";
            $data['title']        = $seminar->title;
            $data['description']  = $seminar->description;
            $data['price']        = $price;
            $data['address']      = $seminar->address;
            $data['latitude']     = $seminar->latitude;
            $data['longitude']    = $seminar->longitude;
            $data['date']         = $seminar->date;
            $data['start_time']   = $seminar->start_time;
            $data['end_time']     = $seminar->end_time;
            $data['tags']         = $seminar->tags;
            
            $title                = strtolower(str_replace(" ", "-", $seminar->title));
            $data['link']         = $this->frontEndUrl."seminar/".$title;
            
            $data['image']        = $seminar->image == null ? "" : $this->cdn."/seminars/".$seminar->image;
            $data['created_at']   = date_format(date_create($seminar->created_at), 'Y-m-d  H:i:s');
            $data['updated_at']   = date_format(date_create($seminar->updated_at), 'Y-m-d  H:i:s');

            $speaker = Speaker::where('seminar_id', $seminar->id)->get();

            foreach ($speaker as $key2 => $value2) {
                $data['speakers'][$key2]['id']            = $value2->id;
                $data['speakers'][$key2]['name']          = $value2->name;
                $data['speakers'][$key2]['title']         = $value2->title;
                $data['speakers'][$key2]['material']      = $value2->seminar_material;
                $data['speakers'][$key2]['created_at']    = date_format(date_create($value2->created_at), 'Y-m-d  H:i:s');
                $data['speakers'][$key2]['updated_at']    = date_format(date_create($value2->updated_at), 'Y-m-d  H:i:s');
            }
        }

        $message = "Berhasil mengambil data detail seminar";

        if(!$seminar) {
            $message = !isset($seminar) ? "Data seminar tidak tersedia." : "Gagal mengambil data detail seminar";
        }

        return response()->json([ 'message' => $message, 'data' => $data ]);
    }
}
