<?php

namespace App\Http\Controllers\Homepage;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Models\Seminar\Seminar;
use App\Models\Seminar\Speaker;
use App\Models\Seminar\Price;
use App\Models\Seminar\Category;
use App\Models\Master\Instantion;
use App\Models\Master\City;
use App\Models\News\News;
use App\Models\Banner\Banner;

class HomepageController extends BaseController
{
    public function __construct()
    {
        $this->cdn = config('app.cdn');
    }

    public function web(Request $request)
    {
        $categories                                 = Category::all();
        $seminars                                   = Seminar::orderBy('created_at', 'DESC')->limit(6)->get();
        $instantions                                = Instantion::orderBy('created_at', 'DESC');

        //if use search on homepage with city
        if($request->has('city') && $request->city != NULL) {
            $city_id = City::where('name','like','%'. $request->city .'%')->pluck('id');
            
            if(sizeof($city_id) > 0) {
                $instantions->whereIn('city_id', $city_id);
            }
        }

        $instantions = $instantions->get();

        $data['categories']                         = array();
        $data['seminars']                           = array();
        $data['instantions']                        = array();

        if(sizeof($instantions) > 0) {
            foreach ($instantions as $key => $value) {
                $data['instantions'][$key]['id']         = $value->id;
                $data['instantions'][$key]['name']       = $value->name;
                $data['instantions'][$key]['alias']      = $value->name_alias;
                $data['instantions'][$key]['image']      = $this->cdn."instantions/".$value->image;
                $data['instantions'][$key]['created_at'] = date_format(date_create($value->created_at), 'Y-m-d  H:i:s');
                $data['instantions'][$key]['updated_at'] = date_format(date_create($value->updated_at), 'Y-m-d  H:i:s');
            }
        }

        if(sizeof($categories) > 0) {
            foreach ($categories as $key => $value) {
                $data['categories'][$key]['id']         = $value->id;
                $data['categories'][$key]['name']       = $value->category_name;
                $data['categories'][$key]['image']      = $this->cdn."seminar_categories/".$value->image;
                $data['categories'][$key]['created_at'] = date_format(date_create($value->created_at), 'Y-m-d  H:i:s');
                $data['categories'][$key]['updated_at'] = date_format(date_create($value->updated_at), 'Y-m-d  H:i:s');
            }
        }

        if(sizeof($seminars) > 0) {
            foreach ($seminars as $key => $value) {

                //get price
                $price          = null;
                $seminarPrice   = Price::where('seminar_id', $value->id)->get();

                if($seminarPrice) {
                    foreach ($seminarPrice as $index => $item) {
                        $price[$index]['id']    = $item->id;
                        $price[$index]['type']  = $item->type;
                        $price[$index]['price'] = $item->price;
                        $price[$index]['label'] = $item->description;
                    }
                }

                $data['seminars'][$key]['id']           = $value->id;
                $data['seminars'][$key]['organizer']    = isset($value->organizer) ? $value->organizer->fullname : "";
                $data['seminars'][$key]['category']     = isset($value->category) ? $value->category->category_name : "";
                $data['seminars'][$key]['title']        = $value->title;
                $data['seminars'][$key]['description']  = $value->description;
                $data['seminars'][$key]['price']        = $price;
                $data['seminars'][$key]['address']      = $value->address;
                $data['seminars'][$key]['latitude']     = $value->latitude;
                $data['seminars'][$key]['longitude']    = $value->longitude;
                $data['seminars'][$key]['date']         = $value->date;
                $data['seminars'][$key]['start_time']   = $value->start_time;
                $data['seminars'][$key]['end_time']     = $value->end_time;
                $data['seminars'][$key]['tags']         = $value->tags;
                $data['seminars'][$key]['image']        = $value->image == null ? "" : $this->cdn."seminars/".$value->image;
                $data['seminars'][$key]['created_at']   = date_format(date_create($value->created_at), 'Y-m-d  H:i:s');
                $data['seminars'][$key]['updated_at']   = date_format(date_create($value->updated_at), 'Y-m-d  H:i:s');

                $speaker = Speaker::where('seminar_id', $value->id)->get();

                foreach ($speaker as $key2 => $value2) {
                    $data['seminars'][$key]['speakers'][$key2]['id']            = $value2->id;
                    $data['seminars'][$key]['speakers'][$key2]['name']          = $value2->name;
                    $data['seminars'][$key]['speakers'][$key2]['title']         = $value2->title;
                    $data['seminars'][$key]['speakers'][$key2]['material']      = $value2->seminar_material;
                    $data['seminars'][$key]['speakers'][$key2]['created_at']    = date_format(date_create($value2->created_at), 'Y-m-d  H:i:s');
                    $data['seminars'][$key]['speakers'][$key2]['updated_at']    = date_format(date_create($value2->updated_at), 'Y-m-d  H:i:s');
                }
            }
        }

        return response()->json([ 'message' => 'Berhasil mengambil data beranda', 'data' => $data ]);
    }

    public function mobile(Request $request)
    {
        $seminars                                   = Seminar::orderBy('created_at', 'DESC')->limit(6)->get();
        $banners                                    = News::all();
        $instantions                                = Instantion::orderBy('created_at', 'DESC');

        //if use search on homepage with city
        if($request->has('city') && $request->city != NULL) {
            $city_id = City::where('name','like','%'. $request->city .'%')->pluck('id');
            
            if(sizeof($city_id) > 0) {
                $instantions->whereIn('city_id', $city_id);
            }
        }

        $instantions = $instantions->get();

        $data['seminars']                           = array();
        $data['instantions']                        = array();
        $data['banners']                            = array();

        if(sizeof($instantions) > 0) {
            foreach ($instantions as $key => $value) {
                $data['instantions'][$key]['id']         = $value->id;
                $data['instantions'][$key]['name']       = $value->name_alias;
                $data['instantions'][$key]['image']      = $this->cdn."instantions/".$value->image;
                $data['instantions'][$key]['created_at'] = date_format(date_create($value->created_at), 'Y-m-d  H:i:s');
                $data['instantions'][$key]['updated_at'] = date_format(date_create($value->updated_at), 'Y-m-d  H:i:s');
            }
        }

        if(sizeof($seminars) > 0) {
            foreach ($seminars as $key => $value) {

                //get price
                $price          = null;
                $seminarPrice   = Price::where('seminar_id', $value->id)->get();

                if($seminarPrice) {
                    foreach ($seminarPrice as $index => $item) {
                        $price[$index]['id']    = $item->id;
                        $price[$index]['type']  = $item->type;
                        $price[$index]['price'] = $item->price;
                        $price[$index]['label'] = $item->description;
                    }
                }

                $data['seminars'][$key]['id']           = $value->id;
                $data['seminars'][$key]['organizer']    = isset($value->organizer) ? $value->organizer->fullname : "";
                $data['seminars'][$key]['category']     = isset($value->category) ? $value->category->category_name : "";
                $data['seminars'][$key]['title']        = $value->title;
                $data['seminars'][$key]['description']  = $value->description;
                $data['seminars'][$key]['price']        = $price;
                $data['seminars'][$key]['address']      = $value->address;
                $data['seminars'][$key]['latitude']     = $value->latitude;
                $data['seminars'][$key]['longitude']    = $value->longitude;
                $data['seminars'][$key]['date']         = $value->date;
                $data['seminars'][$key]['start_time']   = $value->start_time;
                $data['seminars'][$key]['end_time']     = $value->end_time;
                $data['seminars'][$key]['tags']         = $value->tags;
                $data['seminars'][$key]['image']        = $value->image == null ? "" : $this->cdn."seminars/".$value->image;
                $data['seminars'][$key]['created_at']   = date_format(date_create($value->created_at), 'Y-m-d  H:i:s');
                $data['seminars'][$key]['updated_at']   = date_format(date_create($value->updated_at), 'Y-m-d  H:i:s');

                $speaker = Speaker::where('seminar_id', $value->id)->get();

                foreach ($speaker as $key2 => $value2) {
                    $data['seminars'][$key]['speakers'][$key2]['id']            = $value2->id;
                    $data['seminars'][$key]['speakers'][$key2]['name']          = $value2->name;
                    $data['seminars'][$key]['speakers'][$key2]['title']         = $value2->title;
                    $data['seminars'][$key]['speakers'][$key2]['material']      = $value2->seminar_material;
                    $data['seminars'][$key]['speakers'][$key2]['created_at']    = date_format(date_create($value2->created_at), 'Y-m-d  H:i:s');
                    $data['seminars'][$key]['speakers'][$key2]['updated_at']    = date_format(date_create($value2->updated_at), 'Y-m-d  H:i:s');
                }
            }
        }

        if(sizeof($banners) > 0) {
            foreach ($banners as $key => $value) {
                $data['banners'][$key]['id']         = $value->id;
                $data['banners'][$key]['title']      = $value->title;
                $data['banners'][$key]['subtitle']   = substr($value->content,0,100);
                $data['banners'][$key]['image']      = $value->image == null ? "" : $this->cdn."news/".$value->image;
                $data['banners'][$key]['created_at'] = date_format(date_create($value->created_at), 'Y-m-d  H:i:s');
                $data['banners'][$key]['updated_at'] = date_format(date_create($value->updated_at), 'Y-m-d  H:i:s');
            }
        }

        return response()->json([ 'message' => 'Berhasil mengambil data beranda', 'data' => $data ]);
    }
}
