<?php

namespace App\Http\Controllers\Transaction;

use Illuminate\Http\Request;
use App\Models\Transaction\Voucher;
use App\Models\Transaction\Transaction;
use App\Models\Transaction\TransactionDetail;
use App\Models\Seminar\Seminar;
use App\Models\Seminar\Price;
use App\Models\Master\Payment;
use App\User;
use Laravel\Lumen\Routing\Controller as BaseController;

class TransactionController extends BaseController
{
    public function __construct()
    {
        $this->cdn          = config('app.cdn');
        $this->user         = \Auth::user();

        $this->emailUrl     = "https://cms.maleskuliah.com/email/";
    }

    public function index(Request $request)
    {
        $data           = array();

        $transaction    = Transaction::where('email', $request->email)->paginate(6);

        if($this->user)
        {
            $transaction    = Transaction::where('buyer_id', $this->user->id)->paginate(6);
        }

        if(count($transaction) > 0) {
            foreach ($transaction as $key => $value) {

                //seminar
                $seminar = null;

                if(isset($value->seminar))
                {
                    $seminar['id']           = $value->seminar->id;
                    $seminar['title']        = $value->seminar->title;
                    $seminar['date']         = $value->seminar->date;
                    $seminar['start_time']   = $value->seminar->start_time;
                    $seminar['end_time']     = $value->seminar->end_time;
                    $seminar['address']      = $value->seminar->address;
                    $seminar['created_at']   = date_format(date_create($value->created_at), 'Y-m-d  H:i:s');
                    $seminar['updated_at']   = date_format(date_create($value->updated_at), 'Y-m-d  H:i:s');
                }

                //buyer
                $buyer = null;

                if(isset($value->buyer))
                {
                    $buyer['id']           = $value->buyer->id;
                    $buyer['name']         = $value->buyer->fullname;
                    $buyer['email']        = $value->buyer->email;
                    $buyer['phone']        = $value->buyer->phone;
                } else {
                    $buyer['email']        = $request->email;
                }

                //payment
                $payment = null;

                if(isset($value->payment))
                {
                    $payment['id']           = $value->payment->id;
                    $payment['name']         = $value->payment->name;
                    $payment['alias']        = $value->payment->alias;
                    $payment['norek']        = $value->payment->norek;
                }

                $status = "";

                switch ($value->status) {
                    case 0:
                        $status = "Belum Dibayar";
                        break;
                    case 1:
                        $status = "Menunggu Konfirmasi";
                        break;
                    case 2:
                        $status = "Telah Dikonfirmasi";
                        break;
                    case 3:
                        $status = "Selesai";
                        break;
                    case 4:
                        $status = "Tidak Valid";
                        break;

                    default:
                        $status = "";
                        break;
                }

                $data[$key]['id']         = $value->id;
                $data[$key]['code']       = $value->transaction_code;
                $data[$key]['seminar']    = $seminar;
                $data[$key]['buyer']      = $buyer;
                $data[$key]['status']     = $status;
                $data[$key]['payment']    = $payment;

                //participants
                $participants             = array();
                $transaction_detail       = TransactionDetail::where('transaction_id', $value->id)->get();

                //discount                 
                $discount                 = 0;

                if(isset($value->voucher_id) || $value->voucher_id != "")
                {
                    $voucher = Voucher::find($value->voucher_id);
                    $discount = $voucher->nominal;
                }

                $data[$key]['price']      = ($value->price * count($transaction_detail)) + $value->price_unique - $discount;
                
                if($transaction_detail)
                {
                    foreach ($transaction_detail as $key2 => $value2) {
                        $data[$key]['participants'][$key2]['id']    = $value2->id;
                        $data[$key]['participants'][$key2]['name']  = $value2->fullname;
                        $data[$key]['participants'][$key2]['email'] = $value2->email;
                        $data[$key]['participants'][$key2]['phone'] = $value2->phone;
                        $data[$key]['participants'][$key2]['attend'] = $value2->status == 1 ? true : false;
                    }
                    $data[$key]['total_participants'] = count($transaction_detail);
                }

                $data[$key]['created_at'] = date_format(date_create($value->created_at), 'Y-m-d H:i:s');
                $data[$key]['updated_at'] = date_format(date_create($value->updated_at), 'Y-m-d H:i:s');
                $data[$key]['total_page'] = count($transaction);
            }
        }

        $message    = count($data) == 0 ? "Data transaksi tidak tersedia." : "Berhasil mengambil data transaksi";

        if(!$transaction) {
            $message = "Gagal mengambil data transaksi";
        }
        return response()->json([ 'message' => $message, 'current_page' => $transaction->currentPage(), 'data' => $data, 'total' => $transaction->total() ]);
    }

    public function detail($id)
    {
        $data           = null;
        $transaction    = Transaction::find($id);

        $message        = "Detil transaksi tidak tersedia";

        if($transaction) {
            //seminar
            $seminar = null;

            if(isset($transaction->seminar))
            {
                $seminar['id']           = $transaction->seminar->id;
                $seminar['title']        = $transaction->seminar->title;
                $seminar['date']         = $transaction->seminar->date;
                $seminar['start_time']   = $transaction->seminar->start_time;
                $seminar['end_time']     = $transaction->seminar->end_time;
                $seminar['address']      = $transaction->seminar->address;
                $seminar['created_at']   = date_format(date_create($transaction->created_at), 'Y-m-d  H:i:s');
                $seminar['updated_at']   = date_format(date_create($transaction->updated_at), 'Y-m-d  H:i:s');
            }

            //buyer
            $buyer = null;

            if(isset($transaction->buyer))
            {
                $buyer['id']           = $transaction->buyer->id;
                $buyer['name']         = $transaction->buyer->fullname;
                $buyer['email']        = $transaction->buyer->email;
                $buyer['phone']        = $transaction->buyer->phoen;
            } else {
                $buyer['email']        = $transaction->email;
            }

            //payment
            $payment = null;

            if(isset($transaction->payment))
            {
                $payment['id']           = $transaction->payment->id;
                $payment['name']         = $transaction->payment->name;
                $payment['alias']        = $transaction->payment->alias;
                $payment['norek']        = $transaction->payment->norek;
            }

            $status = "";

            switch ($transaction->status) {
                case 0:
                    $status = "Belum Dibayar";
                    break;
                case 1:
                    $status = "Menunggu Konfirmasi";
                    break;
                case 2:
                    $status = "Telah Dikonfirmasi";
                    break;
                case 3:
                    $status = "Selesai";
                    break;
                case 4:
                    $status = "Tidak Valid";
                    break;

                default:
                    $status = "";
                    break;
            }

            $data['id']         = $transaction->id;
            $data['code']       = $transaction->transaction_code;
            $data['seminar']    = $seminar;
            $data['buyer']      = $buyer;
            $data['status']     = $status;
            $data['payment']    = $payment;

            //participants
            $participants             = array();
            $transaction_detail       = TransactionDetail::where('transaction_id', $transaction->id)->get();

            //discount                 
            $discount                 = 0;

            if(isset($transaction->voucher_id) || $transaction->voucher_id != "")
            {
                $voucher = Voucher::find($transaction->voucher_id);
                $discount = $voucher->nominal;
            }

            $data['price']      = ($transaction->price * count($transaction_detail)) + $transaction->price_unique - $discount;

            if($transaction_detail)
            {
                foreach ($transaction_detail as $key2 => $value2) {
                    $data['participants'][$key2]['id']    = $value2->id;
                    $data['participants'][$key2]['name']  = $value2->fullname;
                    $data['participants'][$key2]['email'] = $value2->email;
                    $data['participants'][$key2]['phone'] = $value2->phone;
                    $data['participants'][$key2]['attend'] = $value2->status == 1 ? true : false;
                }
                $data['total_participants'] = count($transaction_detail);
            }

            $data['created_at'] = date_format(date_create($transaction->created_at), 'Y-m-d H:i:s');
            $data['updated_at'] = date_format(date_create($transaction->updated_at), 'Y-m-d H:i:s');


            $message        = "Berhasil mengambil data transaksi ". $transaction->transaction_code;
        }

        return response()->json([ 'message' => $message, 'data' => $data ]);
    }

    public function checkout(Request $request)
    {
        try {
            \DB::beginTransaction();

                $transaction    = New Transaction;
                $detail         = New TransactionDetail;

                $user           = \Auth::user();

                $prefix_code    = "MLS".date('dmY')."-";
                $transaction->transaction_code  = $this->code($prefix_code);
                $participants               = $request->participants;

                if($user)
                {
                    $transaction->buyer_id      = $user->id;
                    $transaction->fullname      = $user->fullname;
                    $transaction->email         = $user->email;
                    $transaction->phone         = $user->phone;
                } else {
                    $transaction->email         = $request->email;
                    $transaction->fullname      = $participants[0]['fullname'];
                    $transaction->phone         = $participants[0]['phone'];
                }

                //get seminar
                $seminar                    = Seminar::find($request->seminar_id);
                //get seminar price         
                $seminarPrice               = Price::where('seminar_id', $request->seminar_id)->count();

                //get code uniqe
                $unique                     = substr(str_shuffle("0123456789"), 0, 3);

                $transaction->seminar_id    = $request->seminar_id;
                $transaction->payment_id    = $request->payment_id;
                
                if($seminarPrice == 0){
                    $transaction->status        = 3;
                } else {
                    $transaction->status        = 0;
                }
                
                //voucher id
                if($request->has('voucher_id') && $request->voucher_id != "")
                {
                    $transaction->voucher_id = $request->voucher_id;
                }

                //if type student price
                if($request->type == 'Student')
                {
                    if($request->hasFile('image'))
                    {
                        $transaction->image_card_identity = $this->uploadImage($request);
                    }
                    $transaction->type          = 1;

                    //get price seminar student 
                    $studentPrice               = Price::where('seminar_id', $seminar->id)
                                                    ->where('type', $request->type)->first();

                    $transaction->price         = $studentPrice->price;
                    $transaction->price_unique  = $unique > 500 ? 500 : $unique;
                } 
                elseif($request->type == 'General') {
                    $transaction->type          = 0;

                    //get price seminar general 
                    $generalPrice               = Price::where('seminar_id', $seminar->id)
                                                    ->where('type', $request->type)->first();

                    $transaction->price         = $generalPrice->price;
                    $transaction->price_unique  = $unique > 500 ? 500 : $unique;
                }

                //if free seminar
                if($seminarPrice == 0){
                    $transaction->price         = 0;
                    $transaction->price_unique  = 0;
                    $transaction->type          = 2; //free seminar
                }

                $transaction->save();

                //detail
                if(count($participants) > 0)
                {
                    foreach ($participants as $key => $value) {
                        $detail                     = New TransactionDetail;
                        $detail->transaction_id     = $transaction->id;
                        $detail->fullname           = $value['fullname'];
                        $detail->email              = $value['email'];
                        $detail->phone              = $value['phone'];
                        $detail->save();
                    }
                }

            \DB::commit();

            //send fcm
            if($user && $user->token_fcm != "") {
                $title      = "Konfirmasi Pemesanan Tiket Seminar";
                $message    = "Anda berhasil melakukan pemesanan tiket seminar. Anda harus melakukan pembayaran tiket seminar agar pemesanan tiket seminar dapat diproses";
                
                if($seminarPrice == 0 ){
                    $title  = "Pemesanan Tiket Seminar Gratis";
                    $message = "Anda berhasil melakukan pemesanan tiket seminar gratis.";
                }

                $action     = "seminar";
                $id         = $transaction->id;
                $token      = $user->token_fcm;
                
                $this->send_fcm($title, $message, $action, $id, $token);

                //insert to notification
                $notification           = new \App\Models\Notification\Notification;
                $notification->title    = $title;
                $notification->content  = $message;
                $notification->is_read  = 0;
                
                if($user) {
                    $notification->user_id  = $user->id;
                }

                $notification->save();
            }

            //end send fcm

            $message        = "Anda berhasil melakukan pemesanan tiket seminar. Anda harus melakukan pembayaran tiket seminar agar pemesanan tiket seminar dapat diproses";
            
            //if free seminar
            if($seminarPrice == 0 ){
                $message    = "Anda berhasil melakukan pemesanan tiket seminar gratis.";
            }
            
            $data           = Transaction::find($transaction->id);
            
            $discount       = 0;

            if(isset($data->voucher_id)){
                $voucher = Voucher::find($data->voucher_id);
                $discount = $voucher->nominal;
            }

            $data['price']  = ( ($data->price * count($participants))  + $data->price_unique) - $discount;

            //get master payment
            $payment    = Payment::find($request->payment_id);

            if($payment) {
                $data['payment']['id']         = $payment->id;
                $data['payment']['name']       = $payment->name;
                $data['payment']['alias']      = $payment->alias;
                $data['payment']['norek']      = $payment->norek;
                $data['payment']['image']      = $this->cdn."payments/".$payment->image;
                $data['payment']['created_at'] = date_format(date_create($payment->created_at), 'Y-m-d H:i:s');
                $data['payment']['updated_at'] = date_format(date_create($payment->updated_at), 'Y-m-d H:i:s');
            } else {
                $data['payment'] = null;
            }

            //send email
            $params = '?email='.$request->email;

            if($user){
                $params = '?email='.$user->email;
            }
 
            $apiUrl = $this->emailUrl."confirmation/".$transaction->transaction_code.$params;

            //if free
            if($seminarPrice == 0 ){
                $apiUrl = $this->emailUrl."freeseminar/".$transaction->transaction_code.$params;
            }
 
             $ch = curl_init();
             curl_setopt($ch, CURLOPT_URL, $apiUrl);
             curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
             curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
             curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
             $result = curl_exec($ch);
 
             curl_close($ch);
 
             //end send email

            return response()->json([ 'message' => $message, 'data' => $data ]);

        } catch(\Exception $e)
        {
            return response()->json([ 'message' => $e->getMessage() ." - ". $e->getLine(), 'status' => false ], 500);
        }
    }

    public function confirmation(Request $request, $id)
    {
        try {
            $transaction         = Transaction::find($id);

            if($transaction->status == 1)
            {
                return response()->json([ 'message' => 'No Transaksi ini sudah melakukan konfirmasi' ], 403);
            } else {
                \DB::beginTransaction();

                    $validator = $this->validate($request, [
                        'date'             => 'required|date',
                        'ammount'          => 'required|integer',
                        'bank'             => 'required|string',
                        'message'          => 'string',
                        'rekening_name'    => 'required|string',
                        'image'            => 'required',
                    ]);

                    $transaction->status = 1;

                    if($request->hasFile('image'))
                    {
                        $transaction->image_payment = $this->uploadImage($request);
                    }

                    $transaction->save();

                \DB::commit();

                //send email
                $apiUrl = $this->emailUrl."paymentconfirm/".$transaction->transaction_code;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $apiUrl);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $result = curl_exec($ch);
                
                curl_close($ch);

                //end send email

                return response()->json([
                    'message' => 'Berhasil melakukan konfirmasi transaksi, mohon menunggu informasi dari admin',
                    'data' => array('transaction_code' => $transaction->transaction_code)
                ]);
            }
        } 
        catch(\Exception $e)
        {
            echo $e->getMessage()." - ".$e->getLine();
        }
    }

    public function scan(Request $request)
    {
        $data        = null;
        $code        = 404;
        $message     = "Data peserta seminar tidak tersedia";
        $transaction = Transaction::where('transaction_code', $request->transaction_code)->first();

        if($transaction)
        {
            $message = "Seminar ini belum melakukan pembayaran, silahkan melakukan pembayaran terlebih dahulu";
            $code    = 422;

            if($transaction->seminar->instantion_id == \Auth::user()->instantion_id) {
                if($transaction->status == 3)
                {
                    $transaction_detail = TransactionDetail::where('transaction_id', $transaction->id)->get();

                    $data['transaction_code'] = $transaction->transaction_code;

                    if(isset($transaction->seminar))
                    {
                        $data['seminar'] = array(
                            'id' => $transaction->seminar->id,
                            'title' => $transaction->seminar->title,
                        );
                    }

                    foreach ($transaction_detail as $key => $value) {
                        $data['participants'][$key]['id']       = $value->id;
                        $data['participants'][$key]['name']     = $value->fullname;
                        $data['participants'][$key]['email']    = $value->email;
                        $data['participants'][$key]['phone']    = $value->phone;
                    }

                    $message = "Berhasil mengambil data peserta seminar";
                    $code    = 200;
                }   
            } else {
                $message = "Peserta ini tidak terdaftar dalam Seminar.";
                $code    = 422;
            }

        }

        return response()->json([ 'message' => $message , 'data' => $data], $code);
    }

    public function absent(Request $request, $id)
    {
        $code           = 500;
        $message        = "Gagal melakukan absen peserta";
        $transaction    = Transaction::where('transaction_code',$id)->first();

        if($transaction)
        {
            $transaction_detail = TransactionDetail::whereIn('id', $request->participants)->update([
                'status' => 1
            ]);
            $message        = "Berhasil melakukan absen peserta";
            $code    = 200;
        }

        return response()->json([ 'message' => $message], $code);
    }

    public function participant(Request $request, $id)
    {
        try {
            $data        = null;
            $message     = "Data peserta seminar tidak tersedia";
            $transaction = Transaction::where('seminar_id', $id)->first();

            if($transaction)
            {
                $transaction_id     = Transaction::where('seminar_id', $id)->where('status',3)->pluck('id');
                $attendees          = TransactionDetail::whereIn('transaction_id', $transaction_id)
                                        ->where('status', 1)->count();

                $transaction_detail = TransactionDetail::whereIn('transaction_id', $transaction_id)->get();

                $data['transaction_code']   = $transaction->transaction_code;

                if(isset($transaction->seminar))
                {
                    $data['seminar'] = array(
                        'id' => $transaction->seminar->id,
                        'title' => $transaction->seminar->title,
                    );
                }

                $data['attendees']          = $attendees;
                $data['total_participant']  = count($transaction_detail);

                $data['participants']       = array();

                foreach ($transaction_detail as $key => $value) {
                    $data['participants'][$key]['id']       = $value->id;
                    $data['participants'][$key]['name']     = $value->fullname;
                    $data['participants'][$key]['email']    = $value->email;
                    $data['participants'][$key]['phone']    = $value->phone;
                    $data['participants'][$key]['attend']   = $value->status == 1 ? true : false;
                    $data['participants'][$key]['time']     = date('Y-m-d H:i:s', strtotime($value->created_at));
                }

                $message = "Berhasil mengambil data peserta seminar";
            }

            return response()->json([ 'message' => $message , 'data' => $data]);
        } catch (\Exception $e)
        {
            return response()->json([ 'message' => $e->getMessage()." - ".$e->getLine()], 422);
        }
    }

    public function customer()
    {
        $customer = TransactionDetail::select(
                            'fullname',
                            'email',
                            'phone'
                        )
                        ->groupBy('fullname')->groupBy('email')
                        ->groupBy('phone')->get();
        $data     = array();

        if(count($customer) > 0)
        {
            foreach ($customer as $key => $value) {
                $data[$key]['name']     = $value->fullname;
                $data[$key]['email']    = $value->email;
                $data[$key]['phone']    = $value->phone;
            }
        }

        $message = count($customer) != 0 ? "Berhasil mengambil data pelanggan" : "Data pelanggan kosong";

        return response()->json([ 'message' => $message , 'data' => $data]);
    }

    public function voucher()
    {
        $data       = array();
        $voucher    = Voucher::all();

        if(count($voucher) > 0) {
            foreach ($voucher as $key => $value) {
                $data[$key]['id']         = $value->id;
                $data[$key]['code']       = $value->code;
                $data[$key]['nominal']    = $value->nominal;
                $data[$key]['max_use']    = $value->max_use;
                $data[$key]['start_date'] = $value->start_date;
                $data[$key]['end_date']   = $value->end_date;
                $data[$key]['created_at'] = date_format(date_create($value->created_at), 'Y-m-d H:i:s');
                $data[$key]['updated_at'] = date_format(date_create($value->updated_at), 'Y-m-d H:i:s');
            }
        }

        $message    = count($data) == 0 ? "Data voucher tidak tersedia." : "Berhasil mengambil data voucher";

        if(!$voucher) {
            $message = "Gagal mengambil data voucher";
        }

        return response()->json([ 'message' => $message, 'data' => $data ]);
    }

    public function reedem(Request $request)
    {
        //cek ketersediaan voucher
        $data    = array();
        $voucher = Voucher::where('code', $request->code)->first();

        if($voucher)
        {
            //check max pemakaian
            $max = Transaction::where('voucher_id', $voucher->id);

            //check if login or guest
            if($this->user) {
                $max->where('buyer_id', $this->user->id);
            } else {
                $max->where('email', $request->email);
            }

            $max = $max->count();

            if($max >= $voucher->max_use) {
                $message = "Akun Anda telah melebihi penggunaan voucher";
                $status  = 500;
                $data    = null;
            } else {
                //check kadaluarsa voucher
                $today   = date('Y-m-d H:i:s');
                $expired = Voucher::where('code', $request->code)
                                ->whereRaw("'". $today ."' BETWEEN start_date AND end_date")
                                ->first();

                if($expired)
                {
                    $message = "Berhasil menggunakan voucher";
                    $status  = 200;
                    $data    = array(
                        'id' => $expired->id,
                        'code' => $expired->code,
                        'nominal' => $expired->nominal,
                        'max_use' => $expired->max_use,
                        'start_date' => $expired->start_date,
                        'end_date' => $expired->end_date,
                        'created_at' => date('Y-m-d H:i:s', strtotime($expired->created_at)),
                        'updated_at' => date('Y-m-d H:i:s', strtotime($expired->updated_at)),
                    );
                } else {
                    $message = "Voucher yang digunakan telah kadaluarsa";
                    $status  = 404;
                    $data    = null;
                }
            }

        } else {
            $data    = null;
            $message = "Voucher Tidak Tersedia";
            $status  = 404;
        }

        return response()->json([ 'message' => $message , 'data' => $data], $status);
    }

    function uploadImage($request)
    {
        $image      = $request->file('image');
        $filename   = time() . '.' . $image->getClientOriginalExtension();
        $request->file('image')->move('../../cms/storage/app/public/transaction', $filename);
        return $filename;
    }

    function code($type)
    {
        $result = Transaction::orderBy('id', 'desc');

        $result = $result->first();

        if ($result) {
            $lastNumber = (int) substr($result->transaction_code, strlen($result->transaction_code) - 4, 4);
            $newNumber = $lastNumber + 1;

            if (strlen($newNumber) == 1) {
                $newNumber = '000'.$newNumber;
            } elseif (strlen($newNumber) == 2) {
                $newNumber = '00'.$newNumber;
            } elseif (strlen($newNumber) == 3) {
                $newNumber = '0'.$newNumber;
            } else {
                $newNumber = $newNumber;
            }

            $currMonth = (int)date('m', strtotime($result->transaction_code));
            $currYear = (int)date('y', strtotime($result->transaction_code));
            $nowMonth = (int)date('m');
            $nowYear = (int)date('y');

            if ( ($currMonth < $nowMonth && $currYear == $nowYear) || ($currMonth == $nowMonth && $currYear < $nowYear) ) {
                $newNumber = '0001';
            } else {
                $newNumber = $newNumber;
            }

            $newCode = $type.$newNumber;
        } else {
            $newCode = $type.'0001';
        }

        return $newCode;
    }

    public function send_fcm($title = "", $message = "", $action = "", $id = "", $token = "")
    {
        $fcmUrl     = "https://fcm.googleapis.com/fcm/send";
        $apiKey     = env('FCM_API_KEY');
        $serverKey  = env('FCM_SERVER_KEY');

        $notificationData  = [
            "title"         => $title,
            "message"       => $message,
            "action"        => $action,
            "id"            => $id
        ];

        $fcmNotification = [
            'to'                => $token,
            'priority'          => 'high',
            'delay_while_idle'  => false,
            'time_to_live'      => 0,
            'data'              => $notificationData
        ];

        $fields = json_encode($fcmNotification);

        $headers = [
            'Authorization: key='. $apiKey,
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        $result = curl_exec($ch);

        curl_close($ch);
    }
}
