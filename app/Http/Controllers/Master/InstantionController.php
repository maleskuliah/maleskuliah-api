<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Models\Master\Instantion;
use Laravel\Lumen\Routing\Controller as BaseController;

class InstantionController extends BaseController
{
    public function __construct()
    {
        $this->cdn = config('app.cdn');
    }

    public function index()
    {
        $data       = array();
        $instantion = Instantion::all();

        if(sizeof($instantion) > 0) {
            foreach ($instantion as $key => $value) {
                $data[$key]['id']         = $value->id;
                $data[$key]['name']       = $value->name;
                $data[$key]['image']      = $this->cdn."/instantions/".$value->image;
                $data[$key]['created_at'] = date_format(date_create($value->created_at), 'Y-m-d H:i:s');
                $data[$key]['updated_at'] = date_format(date_create($value->updated_at), 'Y-m-d H:i:s');
            }
        }

        $message    = sizeof($data) == 0 ? "Data instansi tidak tersedia." : "Berhasil mengambil data instansi";

        if(!$instantion) {
            $message = "Gagal mengambil data instansi";
        }

        return response()->json([ 'message' => $message, 'data' => $data ]);
    }
}
