<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Models\Master\Province;
use Laravel\Lumen\Routing\Controller as BaseController;

class ProvinceController extends BaseController
{
    public function index()
    {
        $province   = Province::all();

        $message    = sizeof($province) == 0 ? "Data provinsi tidak tersedia." : "Berhasil mengambil data provinsi";

        if(!$province) {
            $message = "Gagal mengambil data provinsi";
        }

        return response()->json([ 'message' => $message, 'data' => $province ]);
    }
}
