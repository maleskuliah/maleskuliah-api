<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Models\Master\City;
use Laravel\Lumen\Routing\Controller as BaseController;

class CityController extends BaseController
{
    public function index(Request $request)
    {
        $data       = array();
        $cities     = $request->province_id != null ? City::where('province_id', $request->province_id)->get() : City::all();

        if(sizeof($cities) > 0) {
            foreach ($cities as $key => $value) {
                $data[$key]['id']         = $value->id;
                $data[$key]['name']       = $value->name;
                $data[$key]['province']   = isset($value->province) ? $value->province->name : "";
                $data[$key]['created_at'] = date_format(date_create($value->created_at), 'Y-m-d H:i:s');
                $data[$key]['updated_at'] = date_format(date_create($value->updated_at), 'Y-m-d H:i:s');
            }
        }

        $message    = sizeof($data) == 0 ? "Data kota tidak tersedia." : "Berhasil mengambil data kota";

        if(!$cities) {
            $message = "Gagal mengambil data kota";
        }

        return response()->json([ 'message' => $message, 'data' => $data ]);
    }
}
