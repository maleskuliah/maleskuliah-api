<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Models\Master\Payment;
use Laravel\Lumen\Routing\Controller as BaseController;

class PaymentController extends BaseController
{
    public function __construct()
    {
        $this->cdn = config('app.cdn');
    }

    public function index()
    {
        $data       = array();
        $payment    = Payment::all();

        if(sizeof($payment) > 0) {
            foreach ($payment as $key => $value) {
                $data[$key]['id']         = $value->id;
                $data[$key]['name']       = $value->name;
                $data[$key]['alias']      = $value->alias;
                $data[$key]['norek']      = $value->norek;
                $data[$key]['image']      = $this->cdn."payments/".$value->image;
                $data[$key]['created_at'] = date_format(date_create($value->created_at), 'Y-m-d H:i:s');
                $data[$key]['updated_at'] = date_format(date_create($value->updated_at), 'Y-m-d H:i:s');
            }
        }

        $message    = sizeof($data) == 0 ? "Data payment tidak tersedia." : "Berhasil mengambil data payment";

        if(!$payment) {
            $message = "Gagal mengambil data payment";
        }

        return response()->json([ 'message' => $message, 'data' => $data ]);
    }
}
