<?php

namespace App\Http\Controllers\Email;

use Illuminate\Support\Facades\Mail;
use Laravel\Lumen\Routing\Controller as BaseController;

class EmailController extends BaseController
{
    public function index()
    { 
    	$email = "rizkifauzi37@gmail.com";

    	Mail::raw('Email Sent Tes', function($msg) use ($email) {
            $msg->to([$email]);
            $msg->from(['info@maleskuliah.com']);
            $msg->subject('Testing');
        });
    }
}