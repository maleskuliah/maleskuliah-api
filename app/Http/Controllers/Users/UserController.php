<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use App\User;
use Laravel\Lumen\Routing\Controller as BaseController;

class UserController extends BaseController
{
    public function __construct()
    {
        $this->cdn          = config('app.cdn');
        $this->emailUrl     = "https://cms.maleskuliah.com/email/";
        $this->user         = \Auth::user();
    }

    public function store(Request $request)
    {
        $email = User::where('email', $request->email)->count();
        $username = User::where('username', $request->username)->count();

        if($email > 0 || $username > 0 ) {
            return response()->json(['message' => "Username atau email sudah terdaftar."], 404);
        } else {
            $validator = $this->validate($request, [
                'fullname'      => 'required|string|max:255',
                'email'         => 'required|string|email|max:255',
                'username'      => 'required|string|min:3',
                'password'      => 'required|string|min:6',
                'instantion'    => 'required|string|max:100',
                'phone'         => 'required|string|min:12|max:13'
            ]);
    
            $user           = new User;
    
            foreach ($request->all() as $key => $value) {
                $user->{$key}   = $key == "password" ? app('hash')->make($request->{$key}) : $request->{$key};
            }
    
            //add level user
            $user->level_id = 3;
    
            if($user->save()){
                $url    = "https://api.maleskuliah.com/v1/oauth/token";

                $params['client_id']     = 2;
                $params['client_secret'] = "SWD8qtLY7kvmm1aPGXwSt1anV9ZLsZ3LReOdTrzj";
                $params['grant_type']    = "password";
                $params['username']      = $request->username;
                $params['password']      = $request->password;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                
                $result = curl_exec($ch);
                curl_close($ch);

                $response = json_decode($result,true);
                $response['message'] = "Berhasil melakukan pendaftaran";

                return $response;
            }
                // return response()->json(['message' => "Berhasil melakukan pendaftaran"]);
        }
    }

    public function profile()
    {
        $user = \Auth::user();
        $data = null;

        if($user) {

            $instantion = null;

            if(isset($user->get_instantion))
            {
                $instantion['id']   = $user->get_instantion->id;
                $instantion['name'] = $user->get_instantion->name;
            } else {
                $instantion['id'] = null;
                $instantion['name'] = $user->instantion;
            }

            $avatar = "";

            if($user->avatar != null){
                $avatar = $this->cdn."avatar/".$user->avatar;
                if($user->provider != NULL){
                    $avatar = $user->avatar;
                }
            }

            $data['id']         = $user->id;
            $data['name']       = $user->fullname;
            $data['email']      = $user->email;
            $data['username']   = $user->username;
            $data['phone']      = $user->phone;
            $data['instantion'] = $instantion;
            $data['token_fcm']  = $user->token_fcm;
            $data['image']      = $avatar;
            $data['avatar']     = $avatar;
            $data['created_at'] = date_format(date_create($user->created_at), 'Y-m-d H:i:s');
            $data['updated_at'] = date_format(date_create($user->updated_at), 'Y-m-d H:i:s');

            return response()->json(['message' => "Berhasil mengambil data profil", 'data' => $data]);
        }
    }

    public function update(Request $request)
    {
        $validator = $this->validate($request, [
            'fullname'      => 'required|string|max:255',
            'email'         => 'required|string|email|max:255',
            'username'      => 'required|string|min:3',
            'instantion'    => 'required|string|max:100',
            'phone'         => 'required|string|min:12|max:13'
        ]);

        $user               = User::find(\Auth::user()->id);
        $user->fullname     = $request->fullname;
        $user->email        = $request->email;
        $user->username     = $request->username;
        $user->phone        = $request->phone;
        $user->instantion   = $request->instantion;
        $user->save();

        if($request->hasFile('avatar'))
        {
            $user->avatar = $user->provider != "" ? $this->cdn."avatar/".$this->uploadImage($request) : $this->uploadImage($request);
            // $user->avatar = $this->uploadImage($request);
        }

        if($user->save())
            return response()->json(['message' => "Berhasil melakukan perubahan pada profil"]);
    }

    function uploadImage($request)
    {
        $image      = $request->file('avatar');
        $filename   = 'AVA-'.strtolower($request->username).'-'.time() . '.' . $image->getClientOriginalExtension();
        $request->file('avatar')->move('../../cms/storage/app/public/avatar', $filename);
        return $filename;
    }

    public function password(Request $request)
    {
        $validator = $this->validate($request, [
            'password' => 'required|string|min:6|confirmed'
        ]);

        $user               = User::find(\Auth::user()->id);
        $user->password     = app('hash')->make($request->password);

        if($user->save())
            return response()->json(['message' => "Berhasil melakukan perubahan pada kata sandi"]);
    }

    public function newPassword(Request $request)
    {
        $validator = $this->validate($request, [
            'password' => 'required|string|min:6|confirmed'
        ]);

        $user               = User::where('email', $request->email)->first();
        
        if($user)
        {
            $user->password     = app('hash')->make($request->password);

            if($user->save())
                return response()->json(['message' => "Berhasil melakukan perubahan pada kata sandi"]);
        } else {
            return response()->json(['message' => "Email yang dimasukan tidak tersedia di server kami"], 404);
        }
    }

    public function forgotPassword(Request $request)
    {
        $validator = $this->validate($request, [
            'email' => 'required|string|email'
        ]);

        $user = User::where('email', $request->email)->first();

        if($user)
        {
            if($response->status != false) {
                return response()->json(['message' => "Berhasil melakukan permintaan untuk ulangi kata sandi"]);

                //send email

                $params = '?email='.$request->email;
                $apiUrl = $this->emailUrl."forgotpassword".$params;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $apiUrl);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $result = curl_exec($ch);
                curl_close($ch);

                $response = json_decode($result);

                //end send email
            } else {
                return response()->json(['message' => "Gagal melakukan permintaan untuk ulangi kata sandi"], 500);
            }
        } else {
            return response()->json(['message' => "Email yang dimasukan tidak tersedia di server kami"], 404);
        }
    }

    public function updateFcm(Request $request)
    {
        $token              = $request->token;
        $user               = User::find($this->user->id);
        $user->token_fcm    = $request->token;

        if($user->save())
        {
            return response()->json(['message' => 'Berhasil mengubah token fcm Anda']);
        }

        return response()->json(['message' => 'Gagal mengubah token fcm Anda'], 500);
    }

    public function loginSosmed(Request $request)
    {
        $checkUserSosmed = User::where('email', $request->email)
                            ->where('provider', $request->provider)
                            ->count();

        if($checkUserSosmed > 0) {
            $userSosmed = User::where('email', $request->email)->where('provider', $request->provider)->first();
            $userSosmed->password = app('hash')->make($request->email);
            $userSosmed->save();

            $url    = "https://api.maleskuliah.com/v1/oauth/token";

            $params['client_id']     = 2;
            $params['client_secret'] = "SWD8qtLY7kvmm1aPGXwSt1anV9ZLsZ3LReOdTrzj";
            $params['grant_type']    = "password";
            $params['username']      = $request->email;
            $params['password']      = $request->email;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            
            $result = curl_exec($ch);
            curl_close($ch);

            return json_decode($result,true);

        } else {
            $checkAvailable = User::where('email', $request->email)->count();

            if($checkAvailable > 0) {
                return response()->json(['message' => 'Akun dengan email '. $request->email .' sudah pernah mendaftar'], 500);
            } else {
                $userSosmed               = new User;
                $userSosmed->email        = $request->email;
                $userSosmed->name         = $request->name;
                $userSosmed->fullname     = $request->name;
                $userSosmed->username     = $request->firstName."".$request->lastName;
                $userSosmed->idToken      = $request->idToken;
                $userSosmed->authToken    = $request->authToken;
                $userSosmed->provider     = $request->provider;
                $userSosmed->avatar       = $request->photoUrl;
                $userSosmed->level_id     = 3;
                $userSosmed->password     = app('hash')->make($request->email);
                $userSosmed->save();

                $url    = "https://api.maleskuliah.com/v1/oauth/token";

                $params['client_id']     = 2;
                $params['client_secret'] = "SWD8qtLY7kvmm1aPGXwSt1anV9ZLsZ3LReOdTrzj";
                $params['grant_type']    = "password";
                $params['username']      = $request->email;
                $params['password']      = $request->email;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                $result = curl_exec($ch);
                curl_close($ch);

                return json_decode($result,true);
            }
        }
    }
}
