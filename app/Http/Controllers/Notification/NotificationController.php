<?php

namespace App\Http\Controllers\Notification;

use Illuminate\Http\Request;
use App\Models\Notification\Notification;
use Laravel\Lumen\Routing\Controller as BaseController;

class NotificationController extends BaseController
{
    public function __construct()
    {
        $this->user = \Auth::user();
    }

    public function index(Request $request)
    {
        $data         = array();
        $notification = Notification::where('user_id', $this->user->id)->get();

        if(sizeof($notification) > 0)
        {
            foreach ($notification as $key => $value) {
                $data[$key]['id']         = $value->id;
                $data[$key]['title']      = $value->title;
                $data[$key]['content']    = $value->content;
                $data[$key]['is_read']    = $value->is_read == 0 ? false : true;
                $data[$key]['created_at'] = date('Y-m-d H:i:s', strtotime($value->created_at));
                $data[$key]['updated_at'] = date('Y-m-d H:i:s', strtotime($value->updated_at));
            }
        }

        $message = !isset($notification) ? "Data notification tidak tersedia." : "Berhasil mengambil data notification";

        if(!$notification) {
            $message = "Gagal mengambil data notification";
        }

        return response()->json([ 'message' => $message, 'data' => $data ]);
    }

    public function show($id)
    {
        $data                   = null;
        $notification           = Notification::find($id);

        if($notification)
        {
            $data['id']         = $notification->id;
            $data['title']      = $notification->title;
            $data['content']    = $notification->content;
            $data['is_read']    = $notification->is_read == 0 ? false : true;
            $data['created_at'] = date('Y-m-d H:i:s', strtotime($notification->created_at));
            $data['updated_at'] = date('Y-m-d H:i:s', strtotime($notification->updated_at));
        }

        $message = "Berhasil mengambil data detail notification";

        if(!$notification) {
            $message = !isset($notification) ? "Detail notification tidak tersedia." : "Gagal mengambil data detail notification";
        } else {
            $notification->is_read  = 1;
            $notification->save();
        }

        return response()->json([ 'message' => $message, 'data' => $data ]);
    }
}
