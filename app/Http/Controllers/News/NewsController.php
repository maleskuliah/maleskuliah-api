<?php

namespace App\Http\Controllers\News;

use Illuminate\Http\Request;
use App\Models\News\News;
use App\Models\News\Comment;
use Laravel\Lumen\Routing\Controller as BaseController;

class NewsController extends BaseController
{
    public function __construct()
    {
        $this->cdn = config('app.cdn');
        $this->frontEndUrl = "https://maleskuliah.com/";
    }

    public function index(Request $request)
    {
        $data   = array();
        $news   = isset($request->category_id) ? News::where('category_id', $request->category_id)->orderBy('created_at', 'DESC')->get() : News::orderBy('created_at', 'DESC')->get();

        if(sizeof($news) > 0) 
        {
            foreach ($news as $key => $value) {
                $data[$key]['id']               = $value->id;
                $data[$key]['writer']           = isset($value->writer) ? $value->writer->fullname : ""; 
                $data[$key]['category']         = isset($value->category) ? $value->category->category_name : "";
                $data[$key]['title']            = $value->title; 
                $data[$key]['content']          = $value->content; 
                $data[$key]['tags']             = $value->tags;
                $data[$key]['image']            = $value->image == null ? "" : $this->cdn."/news/".$value->image;
                $data[$key]['created_at']       = date_format(date_create($value->created_at), 'Y-m-d H:i:s');
                $data[$key]['updated_at']       = date_format(date_create($value->updated_at), 'Y-m-d H:i:s');

                $comment                        = Comment::where('news_id', $value->id)->count();
                $data[$key]['total_comment']    = $comment;
            }
        }

        $populer   = array();
        $query   = isset($request->category_id) ? 
                        News::where('category_id', $request->category_id)->orderBy('is_view', 'DESC')->limit(5)->get() : 
                        News::orderBy('is_view', 'DESC')->limit(5)->get();

        if(sizeof($query) > 0) 
        {
            foreach ($query as $key => $value) {
                $populer[$key]['id']           = $value->id;
                $populer[$key]['writer']       = isset($value->writer) ? $value->writer->fullname : ""; 
                $populer[$key]['category']     = isset($value->category) ? $value->category->category_name : "";
                $populer[$key]['title']        = $value->title; 
                $populer[$key]['content']      = $value->content;
                $populer[$key]['tags']         = $value->tags; 
                $populer[$key]['image']        = $value->image == null ? "" : $this->cdn."/news/".$value->image;
                $populer[$key]['created_at']   = date_format(date_create($value->created_at), 'Y-m-d H:i:s');
                $populer[$key]['updated_at']   = date_format(date_create($value->updated_at), 'Y-m-d H:i:s');

                $comment                        = Comment::where('news_id', $value->id)->count();
                $populer[$key]['total_comment']    = $comment;
            }
        }

        $message = sizeof($data) == 0 ? "Data berita tidak tersedia." : "Berhasil mengambil data berita";

        if(!$news) {
            $message = "Gagal mengambil data berita";
        }

        return response()->json([ 'message' => $message, 'data' => $data, 'populer' => $populer ]);
    }

    public function show($id)
    {
        $data = null;
        $news = News::find($id);

        if($news) 
        {
            //counter is view
            $news->is_view = $news->is_view + 1;
            $news->save();

            $data['id']           = $news->id;
            $data['writer']       = isset($news->writer) ? $news->writer->fullname : ""; 
            $data['category']     = isset($news->category) ? $news->category->category_name : "";
            $data['title']        = $news->title; 
            $data['content']      = $news->content;

            $title                = strtolower(str_replace(" ", "-", $news->title));

            $data['link']         = $this->frontEndUrl."news/".$title;
            $data['tags']         = $news->tags;
            $data['image']        = $news->image == null ? "" : $this->cdn."/news/".$news->image; 
            $data['created_at']   = date_format(date_create($news->created_at), 'Y-m-d H:i:s');
            $data['updated_at']   = date_format(date_create($news->updated_at), 'Y-m-d H:i:s'); 

            $total_comment            = Comment::where('news_id', $news->id)->count();
            $data['total_comment']    = $total_comment;  

            //comment 
            $comment              = Comment::where('news_id', $id)->get();
            $data['comments']     = array();

            if($comment)
            {
                foreach ($comment as $key => $value) {
                    
                    $image = "";
                    if(isset($value->user)){
                        if(isset($value->user->avatar)){
                            $image = $this->cdn."avatar/".$value->user->avatar;
                            if($value->user->provider != NULL){
                                $image = $value->user->avatar;
                            }
                        }
                    }

                    $data['comments'][$key]['fullname']      = $value->fullname;
                    $data['comments'][$key]['email']         = $value->email;
                    $data['comments'][$key]['message']       = $value->comment;
                    $data['comments'][$key]['image']         = $image;
                    $data['comments'][$key]['created_at']    = date_format(date_create($value->created_at), 'Y-m-d H:i:s');
                    $data['comments'][$key]['updated_at']    = date_format(date_create($value->updated_at), 'Y-m-d H:i:s'); 
                }
            }
        }

        $message = "Berhasil mengambil data detail berita";

        if(!$news) {
            $message = !isset($data) ? "Detail berita tidak tersedia." : "Gagal mengambil data detail berita";
        }

        return response()->json([ 'message' => $message, 'data' => $data ]);
    }

    public function show_web($id)
    {
        $data = null;

        $title   = str_replace("-", " ", ucwords($id));
        $news    = News::where('title','like','%'. $title .'%')->first();

        if($news) 
        {
            //counter is view
            $news->is_view = $news->is_view + 1;
            $news->save();

            $data['id']           = $news->id;
            $data['writer']       = isset($news->writer) ? $news->writer->fullname : ""; 
            $data['category']     = isset($news->category) ? $news->category->category_name : "";
            $data['title']        = $news->title; 
            $data['content']      = $news->content;
            $data['tags']         = $news->tags;

            $title                = strtolower(str_replace(" ", "-", $news->title));
            $data['link']         = $this->frontEndUrl."news/".$title;
            
            $data['image']        = $news->image == null ? "" : $this->cdn."/news/".$news->image; 
            $data['created_at']   = date_format(date_create($news->created_at), 'Y-m-d H:i:s');
            $data['updated_at']   = date_format(date_create($news->updated_at), 'Y-m-d H:i:s'); 

            $total_comment            = Comment::where('news_id', $news->id)->count();
            $data['total_comment']    = $total_comment;  

            //comment 
            $comment              = Comment::where('news_id', $news->id)->get();
            $data['comments']     = array();

            if($comment)
            {
                foreach ($comment as $key => $value) {

                    $image = "";
                    if(isset($value->user)){
                        if(isset($value->user->avatar)){
                            $image = $this->cdn."avatar/".$value->user->avatar;
                            
                            if($value->user->provider != NULL){
                                $image = $value->user->avatar;
                            }
                        }
                    }

                    $data['comments'][$key]['fullname']      = $value->fullname;
                    $data['comments'][$key]['email']         = $value->email;
                    $data['comments'][$key]['message']       = $value->comment;
                    $data['comments'][$key]['image']         = $image;
                    $data['comments'][$key]['created_at']    = date_format(date_create($value->created_at), 'Y-m-d H:i:s');
                    $data['comments'][$key]['updated_at']    = date_format(date_create($value->updated_at), 'Y-m-d H:i:s'); 
                }
            }

            //get popular news
            $query   = News::orderBy('is_view', 'DESC')->limit(5)->get();

            if(sizeof($query) > 0) 
            {
                foreach ($query as $key => $value) {
                    $data['populars'][$key]['id']           = $value->id;
                    $data['populars'][$key]['writer']       = isset($value->writer) ? $value->writer->fullname : ""; 
                    $data['populars'][$key]['category']     = isset($value->category) ? $value->category->category_name : "";
                    $data['populars'][$key]['title']        = $value->title; 
                    $data['populars'][$key]['content']      = $value->content; 
                    $data['populars'][$key]['image']        = $value->image == null ? "" : $this->cdn."/news/".$value->image;
                    $data['populars'][$key]['created_at']   = date_format(date_create($value->created_at), 'Y-m-d H:i:s');
                    $data['populars'][$key]['updated_at']   = date_format(date_create($value->updated_at), 'Y-m-d H:i:s');

                    $comment_populer                        = Comment::where('news_id', $value->id)->count();
                    $data['populars'][$key]['total_comment']    = $comment_populer;
                }
            }
        }

        $message = "Berhasil mengambil data detail berita";

        if(!$news) {
            $message = !isset($data) ? "Detail berita tidak tersedia." : "Gagal mengambil data detail berita";
        }

        return response()->json([ 'message' => $message, 'data' => $data ]);
    }

    public function comment(Request $request, $id)
    {
        $validator = $this->validate($request, [
            'comment'      => 'required|string|max:255',
        ]);

        $user                       = \Auth::user();

        if($user)
        {
            $comment                    = New Comment;
            $comment->news_id           = $id;
            $comment->user_id           = $user->id;
            $comment->fullname          = $user->fullname;
            $comment->email             = $user->email;
            $comment->comment           = $request->comment;
            $comment->comment_tail      = 0;

            $message                    = "Berhasil menambahkan komentar";

            if(!$comment->save())
            {
                $message                = "Gagal menambahkan komentar";
            }
        }

        return response()->json([ 'message' => $message, 'data' => $comment ]);
    }
}
