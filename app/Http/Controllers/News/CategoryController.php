<?php

namespace App\Http\Controllers\News;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\Models\News\Category;

class CategoryController extends BaseController
{
    public function __construct()
    {
        $this->cdn = config('app.cdn');
    }

    public function index()
    {
        $data     = array();
        $category = Category::all();

        if(sizeof($category) > 0) {
            foreach ($category as $key => $value) {
                $data[$key]['id']         = $value->id;
                $data[$key]['name']       = $value->category_name;
                $data[$key]['image']      = $this->cdn."/news_categories/".$value->image;
                $data[$key]['created_at'] = date_format(date_create($value->created_at), 'Y-m-d H:i:s');
                $data[$key]['updated_at'] = date_format(date_create($value->updated_at), 'Y-m-d H:i:s');
            }
        }

        $message    = sizeof($data) == 0 ? "Data kategori berita tidak tersedia." : "Berhasil mengambil data kategori berita";

        if(!$category) {
            $message = "Gagal mengambil data kategori berita";
        }

        return response()->json([ 'message' => $message, 'data' => $data ]);
    }
}
