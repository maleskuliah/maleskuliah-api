<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Models\Cms\Career;
use App\Models\Cms\CareerApply;
use App\Models\Cms\CareerCategory;
use Laravel\Lumen\Routing\Controller as BaseController;

class CareerController extends BaseController
{
	public function __construct()
    {
        $this->cdn = config('app.cdn');
    }

    public function index(Request $request)
    {
        $data       = array();
        $career     = Career::count();
        $category   = CareerCategory::all();

        if($career > 0) {
            foreach ($category as $key1 => $detail) {
                $data[$key1]['category'] = $detail->name;

                $career     = Career::where('category_id', $detail->id)->get();

                $data[$key1]['career'] = array();

                foreach($career as $key => $result)
                {
                    $data[$key1]['career'][$key]['id']           = $result->id;
                    $data[$key1]['career'][$key]['title']        = $result->title;
                    $data[$key1]['career'][$key]['departement']  = $result->departement;
                    $data[$key1]['career'][$key]['industry']     = $result->industry;
                    $data[$key1]['career'][$key]['type']         = $result->type;
                    $data[$key1]['career'][$key]['experience']   = $result->experience;
                    $data[$key1]['career'][$key]['function']     = $result->function;
                    $data[$key1]['career'][$key]['salary']       = $result->salary;
                    $data[$key1]['career'][$key]['description']  = $result->description;
                    $data[$key1]['career'][$key]['location']     = $result->location;
                    $data[$key1]['career'][$key]['created_at']   = date_format(date_create($result->created_at), 'Y-m-d H:i:s');
                    $data[$key1]['career'][$key]['updated_at']   = date_format(date_create($result->updated_at), 'Y-m-d H:i:s');
                }
            }
        	
        	$message    = "Berhasil mengambil data karir";
        } else {
        	$message    = "Data karir tidak tersedia";
        }

        return response()->json([ 'message' => $message, 'data' => $data ]);
    }

    public function show(Request $request, $id)
    {
        $data       = null;
        
        $career        = Career::find($id);

        if($career) {
            $data['id']           = $career->id;
            $data['title']        = $career->title;
            $data['departement']  = $career->departement;
            $data['industry']     = $career->industry;
            $data['type']         = $career->type;
            $data['experience']   = $career->experience;
            $data['function']     = $career->function;
            $data['salary']       = $career->salary;
            $data['description']  = $career->description;
            $data['location']     = $career->location;
            $data['created_at']   = date_format(date_create($career->created_at), 'Y-m-d H:i:s');
            $data['updated_at']   = date_format(date_create($career->updated_at), 'Y-m-d H:i:s');
        	
        	$message    = "Berhasil mengambil data karir";
        } else {
        	$message    = "Data karir tidak tersedia";
        }

        return response()->json([ 'message' => $message, 'data' => $data ]);
    }

    public function apply(Request $request, $id)
    {
        $validator = $this->validate($request, [
            'fullname'      => 'required|string|max:255',
            'email'         => 'required|string|email|max:255',
            'birthday'      => 'required',
            'gender'        => 'required',
            'phone'         => 'required|string|min:12|max:13',
            'address'       => 'required|string',
        ]);

        try {
            $career               = New CareerApply;
            $career->career_id    = $id;
            $career->fullname     = $request->fullname;
            $career->email        = $request->email;
            $career->phone        = $request->phone;
            $career->gender       = $request->gender;
            $career->birthday     = $request->birthday;
            $career->address      = $request->address;
          
            if($request->hasFile('resume')){
                $career->resume     = $this->uploadImage($request);
            }

            if($career->save())
                return response()->json(['message' => "Berhasil melakukan pengajuan lamaran pekerjaan ke pihak Males Kuliah."]);
        } 
        catch(\Exception $e)
        {
            return response()->json(['message' => $e->getMessage()." - ".$e->getLine()." - ".$e->getCode()], 500);
        }
    }

    function uploadImage($request)
    {
        $image      = $request->file('resume');
        $filename   = time() . '.' . $image->getClientOriginalExtension();
        $request->file('resume')->move('../../cms/public/storage/jobs', $filename);
        return $filename;
    }
}
