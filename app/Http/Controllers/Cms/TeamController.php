<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Models\Cms\Team;
use Laravel\Lumen\Routing\Controller as BaseController;

class TeamController extends BaseController
{
	public function __construct()
    {
        $this->cdn = config('app.cdn');
    }

    public function index(Request $request)
    {
        $data       = array();
        
        $team        = Team::all();

        if(sizeof($team) > 0) {
            foreach($team as $key => $result)
            {
                $data[$key]['id']           = $result->id;
                $data[$key]['name']         = $result->fullname;
                $data[$key]['description']  = $result->description;
                $data[$key]['image']        = $this->cdn."/avatar/".$result->image;
                $data[$key]['facebook']     = $result->facebook;
                $data[$key]['twitter']      = $result->twitter;
                $data[$key]['instagram']    = $result->instagram;
                $data[$key]['created_at']   = date_format(date_create($result->created_at), 'Y-m-d H:i:s');
                $data[$key]['updated_at']   = date_format(date_create($result->updated_at), 'Y-m-d H:i:s');
            }
        	
        	$message    = "Berhasil mengambil data tim";
        } else {
        	$message    = "Data tim tidak tersedia";
        }

        return response()->json([ 'message' => $message, 'data' => $data ]);
    }
}
