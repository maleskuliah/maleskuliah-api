<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Models\Cms\Cms;
use Laravel\Lumen\Routing\Controller as BaseController;

class CmsController extends BaseController
{
	public function __construct()
    {
        $this->cdn = config('app.cdn');
    }

    public function index(Request $request)
    {
        $data       = null;
        
        $cms        = Cms::where('alias', $request->title)->first();

        if($cms) {
            $data['id']         = $cms->id;
            $data['title']      = $cms->title;
            $data['content']    = $cms->content;
            $data['image']      = $this->cdn."/cms/".$cms->image;
            $data['created_at'] = date_format(date_create($cms->created_at), 'Y-m-d H:i:s');
            $data['updated_at'] = date_format(date_create($cms->updated_at), 'Y-m-d H:i:s');
        	
        	$message    = "Berhasil mengambil data halaman ". $request->title;
        } else {
        	$message    = "Data halaman ". $request->title ." tidak tersedia";
        }

        return response()->json([ 'message' => $message, 'data' => $data ]);
    }

    public function mobile(Request $request)
    {
        $cms        = Cms::where('alias', $request->title)->first();
        $html       = "<h5 style='text-center'>Halaman tidak tersedia</h5>";

        if($cms)
        {
            $html = "
                <html>
                    <body>
                        <div style='center'>
                            <img src='". $this->cdn.'cms/'.$cms->image."'>
                            ". $cms->content ."
                        </div>
                    </body>
                </html>
            ";
        }

        return $html;
    }
}
