<?php

namespace App\Http\Controllers\Faq;

use Illuminate\Http\Request;
use App\Models\Faq\Faq;
use Laravel\Lumen\Routing\Controller as BaseController;

class FaqController extends BaseController
{
    public function index(Request $request)
    {
        $faq = Faq::orderBy('id', 'DESC')->get();

        $message = sizeof($faq) == 0 ? "Data faq tidak tersedia." : "Berhasil mengambil data faq";

        if(!$faq) {
            $message = "Gagal mengambil data faq";
        }

        return response()->json([ 'message' => $message, 'data' => $faq ]);
    }

    public function show($id)
    {
        $faq = Faq::find($id);

        $message = "Berhasil mengambil data detail faq";

        if(!$faq) {
            $message = sizeof($faq) == 0 ? "Detail faq tidak tersedia." : "Gagal mengambil data detail faq";
        }

        return response()->json([ 'message' => $message, 'data' => $faq ]);
    }
}
