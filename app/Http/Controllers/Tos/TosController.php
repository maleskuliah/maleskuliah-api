<?php

namespace App\Http\Controllers\Tos;

use Illuminate\Http\Request;
use App\Models\Tos\Tos;
use Laravel\Lumen\Routing\Controller as BaseController;

class TosController extends BaseController
{
    public function __construct()
    {
        $this->cdn  = config('app.cdn');
    }
    public function index(Request $request)
    {
        $tos = Tos::all();

        $message = !isset($tos) ? "Data tos tidak tersedia." : "Berhasil mengambil data tos";

        if(!$tos) {
            $message = "Gagal mengambil data tos";
        }

        return response()->json([ 'message' => $message, 'data' => $tos ]);
    }

    public function mobile()
    {
        $tos    = Tos::first();
        $html   = "<h5 style='text-center'>Data Term Of Service tidak tersedia</h5>";

        if($tos)
        {
            $html = "
                <html>
                    <body>
                        <div style='center'>
                            ". $tos->description ."
                        </div>
                    </body>
                </html>
            ";
        }

        return $html;
    }
}
